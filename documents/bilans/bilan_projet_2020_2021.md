# Bilan et projet de l’action fédératrice « Développement logiciel et calcul numérique »


## Organisation et objectifs

L’action fédératrice « Développement logiciel et calcul numérique » (DEVCAL), dont les activités ont démarré au deuxième semestre 2017, a poursuivi son travail depuis. L’AF DEVCAL est coordonnée par un bureau composé de :
* Mickaël Gastineau (IMCCE) ;
* Franck Le Petit (LERMA) ;
* Aurélia Marchand (DIO) ;
* Fabrice Roy (LUTH) ;
* Frédéric Vincent (LESIA).

L’AF DEVCAL a pour objectif d’organiser des ateliers de formation à l’utilisation d’outils et de méthodes de programmation modernes, et ainsi de contribuer à l’amélioration de la qualité des codes de calcul (ou logiciels), quels qu’ils soient, à l’Observatoire de Paris.  
Tous les personnels de l’Observatoire pratiquant la programmation sont a priori concernés, même si certains thèmes sont plutôt réservés aux personnes développant des codes de calcul numérique.  

Les ateliers sont dupliqués pour un même thème à Paris et à Meudon, ce qui permet de toucher plus facilement l’ensemble des personnels de l’Observatoire et d’offrir aux personnes intéressées mais non disponibles lors de l’atelier parisien une chance d’assister à l’atelier meudonnais, ou inversement.  
Les participant.e.s sont invité.e.s à s’inscrire sur la liste de diffusion forum.informatique de l’Observatoire afin d’échanger sur les sujets abordés pendant les ateliers.  

L’AF utilise indico (https://indico.obspm.fr/category/36/) pour organiser les ateliers et déposer les supports. Nous avons également une page internet dédiée à l’action fédératrice
(http://devcal.obspm.fr/) qui présente les actions organisées et recense quelques ressources (sites internet, livres) que nous jugeons utiles pour le développement de logiciels.  

Les ateliers sont annoncés par l’intermédiaire des informaticiens des unités. Nous envoyons un message à la liste netadmin présentant le sujet de l’atelier et demandant aux informaticiens de relayer l’information auprès des membres de leur unité. Nous envoyons également un rappel avant chacun des ateliers.  
Le relais des informaticien.ne.s des unités nous semble important pour plusieurs raisons. Ils sont les mieux placés pour renseigner les personnels sur l’adéquation de l’atelier avec leurs besoins et recommander la participation à l’atelier à quelqu’un qui hésiterait. Nous espérons qu’entretenir un contact avec eux nous permettra de les encourager à participer à l’action en proposant des thèmes d’atelier. Nous souhaitons également qu’ils nous renseignent sur les besoins de formations identifiés dans leur unité.  
Nous utilisons également les listes de diffusion mpopm, forum.informatique et info.mesopsl afin d’optimiser les chances de toucher toutes les personnes potentiellement intéressées. 


## Bilan 2020

L'AF n'a organisé aucun atelier lors de l'année 2020. Nous avons considéré que la situation sanitaire ne permettait pas de réunir les participant.e.s dans une même salle dans des conditions acceptables.  
L'organisation des ateliers par visioconférence nous a paru trop compliquée à mettre en oeuvre. En effet, le principe de ces événements est de permettre aux participant.e.s et à l'animateur.trice d'échanger spontanément et librement, et de travailler si besoin sur le même écran lors des exercices pratiques.  


## Projet pour l’année 2021

Nous prévoyons d’organiser 3 ateliers en 2021 (6 séances au total), mais ce nombre sera réduit si les conditions sanitaires ne s'améliorent pas suffisamment rapidement.  
Les thèmes et le planning seront décidés plus tard, mais nous avons ciblé quelques thèmes potentiellement intéressants parmi lesquels nous choisirons :
* le langage Julia ;
* méthodes numériques et bibliothèques scientifiques (BLAS/lapack, MKL, eigen, etc.) ;
* les fonctions avancées de gitlab (intégration continue, gestion des développements
collaboratifs, etc.) ;
* python avancé ;
* utilisation de Tycho et de MesoPSL.
  
Nous avions déjà commencé à échanger avec des équipes de l'Observatoire qui avaient accepté de participer à l'organisation d'un atelier sur git et gitlab. Ce thème sera donc certainement choisi lorsque nous relancerons les ateliers.  
Nous avions également été sollicités pour organiser une introduction au machine learning, sur un format plus long que ceux de nos ateliers. Une enquête menée auprès des membres de l'Observatoire avait révélé un très fort intérêt, avec environ 70 réponses de personnes intéressées. Un groupe d'organisateurs s'était constitué, et avait commencé à réfléchir au contenu de la formation. Nous allons relancer ce projet pour 2021 en espérant pouvoir le mener à bien.


## Budget

Puisque nous n'avons organisé aucun événement en 2020, nous n'avons pas dépensé le budget qui nous avait été attribué. Nous souhaitons cependant faire une demande de budget pour 2021 en espérant pouvoir organiser 3 atelier (2 séances) et aider à l'organisation de la formation d'introduction au machine learning.  

Ce budget nous permettra, comme lors des années précédentes, d'offrir une pause café aux participant.e.s pendant les ateliers. Nous n’avons pas encore de visibilité claire sur le nombre de participant.e.s moyen, ni sur le nombre et la durée moyenne des ateliers. Si nous nous basons sur les ateliers organisés en 2018 et 2019, et que nous organisons 1 atelier d’une journée (2 pauses) et deux ateliers d'une 1/2 journée (1 pause) en 2021, chacun de ces ateliers ayant lieu une fois à Paris et une fois à Meudon, nous prévoyons un budget de 400 € HT pour ces pauses.  

L'organisation de la formation machine learning pourrait nécessiter un budget plus important, notamment si nous invitons des collègues spécialistes de ce domaine venant d'autres établissements de région parisienne, voire de province. Par ailleurs, cette formation pourrait rassembler un nombre de participant.e.es beaucoup plus important que nos ateliers habituels. Nous ne pensons pas financer intégralement cet événement, puisque d'autres actions déjà financées par l'Observatoire et concernées par le machine learning, comme Minerva, pourraient également participer au financement. Mais nous souhaitons participer à son budget à hauteur de 500 euros.   

Nous aimerions donc bénéficier d'un budget de 1200 euros pour l'année 2021, qui nous permettrait à la fois d'organiser nos ateliers, en invitant si nécessaire un.e collègue pour l'animer, et de participer au financement de la formation sur le machine learning. Toutes nos actions seront dépendantes de la situation sanitaire.
