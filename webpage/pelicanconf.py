#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'AF DEVCAL'
SITENAME = 'AF DEVCAL'
SITEURL = ''

PATH = 'content'
DEFAULT_DATE='fs'
TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

PLUGIN_PATHS = ['./plugins']
PLUGINS = ['pelican_dynamic',]

#THEME = "notmyidea"
#THEME="html5-dopetrope"
THEME="themes/blueidea"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

#MENUITEMS = [ ('AF DEVCAL', '/pages/af-devcal.html'),
#             ('Ressources', '/pages/ressources.html')
#             ]


# Blogroll
LINKS = ( ('Observatoire de Paris', 'http://www.obspm.fr'),
          ('Indico DEVCAL', 'https://indico.obspm.fr/category/36/'),
          ('Rocket Chat', 'https://chat.obspm.fr/channel/devcal'),
          )

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

DISPLAY_PAGES_ON_MENU =True
PAGES_SORT_ATTRIBUTE='title'
LOAD_CONTENT_CACHE = False
PAGES= (( ))
