Title: AF DEVCAL


## Objectif
L'Action Fédératrice Développement Logiciel et Calcul Numérique (AF DEVCAL) a pour objectif de rassembler tous les acteurs de l'Observatoire de Paris qui développent des logiciels pour réaliser des simulations numériques ou des services : doctorants, ingénieurs permanent ou en cdd, chercheurs  expérimentés ou jeunes.  
Le développement de ces logiciels a fortement évolué dans la dernière décennie grâce à l'apparition de nombreux outils facilitant certains de ses aspects. Amener les développeurs à améliorer leur pratique quotidienne de l'écriture de codes en leur présentant ces outils et en montrant leur intérêt pour notre communauté est un enjeu important pour l’Observatoire.


## Moyens
De nombreux doctorants et jeunes chercheurs se forment tous les jours sur le « tas ». Les bonnes méthodes et pratiques du développement pourraient être présentées afin d'améliorer la qualité et la pérennité des codes. Ces deux éléments sont nécessaires afin de produire des résultats rigoureux dans le temps, notamment pour les services, liés aux problèmes d'évolution du hardware et des logiciels des ordinateurs. En effet, les codes sont généralement développés pour des périodes allant de 5 à 20 ans, voir plus. De plus, ces bonnes pratiques facilitent grandement le portage de codes sur les centres de calcul, leur optimisation et leur adoption par la communauté dans le cas des codes ouverts et diffusés.  
L'AF DEVCAL organise donc des ateliers à Paris et à Meudon, au rythme de 4 par an sur chacun des sites, afin de présenter des outils et méthodes adaptés à l'activité du développement dans la recherche académique.


## Bureau
Le bureau de l'action fédératrice est composé de :

* Mickaël Gastineau (IMCCE)
* Franck Le Petit (LERMA)
* Aurélia Marchand (DIO)
* Fabrice Roy (LUTH)
* Frédéric Vincent (LESIA)


## Contact
Si vous souhaitez proposer un sujet d'atelier, si vous avez des demandes/besoins, vous pouvez contacter les membres du bureau en utilisant l'alias mail [bureau.devcal](mailto:bureau(dot)devcal(arobase)obspm(dot)fr).   
Si vous souhaitez échanger autour des sujets abordés lors des ateliers (ou de tout autre sujet lié au dévelopement logiciel et au calcul), nous vous encourageons à le faire sur les listes de diffusion [forum.informatique](https://sympa.obspm.fr/wws/info/forum.informatique) ou [mpopm](https://sympa.obspm.fr/wws/info/mpopm) de l'Observatoire.