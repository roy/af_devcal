Title: Ressources

## Formations au calcul

### Web

* [Groupe Calcul](http://calcul.math.cnrs.fr/)
* [FoCal](http://formation-calcul.fr/)
* [PRACE Training](http://www.training.prace-ri.eu/nc/training_courses/index.html)

## Bonnes pratiques de développement

### Livres

* Code complete, de Steve Mc Connell (Microsoft Press)  
* The pragmatic programmer, de Andrew Hunt et David Thomas (Addison Wesley)

## Algorithmique

### Livres

* Introduction à l'algorithmique, de Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, Clifford Stein (Dunod)

## Fortran

### Livres

* Modern Fortran, de Norman S. Clerman et Walter Spector (Cambridge)
* Modern Fortran explained, de Michael Metcalf, John Reid et Malcolm Cohen (Oxford)

### Web

* [Fortran90.org](http://www.fortran90.org/)
* [Fortran-lang](https://fortran-lang.org/) et son [salon de discussion](https://fortran-lang.discourse.group/)


## C++

### Livres

* Programming, principles and practice using C++, de Bjarne Stroustrup (Addison Wesley)

### Web

* [C++ reference](http://en.cppreference.com/w/)

## Git et gitlab

### Web

* [Documentation officielle de la version "comunity" de gitlab](https://docs.gitlab.com/ee/README.html)