Title: Agenda

## À venir

### 2021

* Présentation des moyens de calcul de l'Observatoire, le mercredi 17 février en visioconférence.
  
## Passé

### 2020

* Aucun événement

### 2019

* [Atelier Docker]({filename}../Ateliers/docker_paris.md), le mercredi 20 novembre à Paris ([informations](https://indico.obspm.fr/event/153/))

* [Atelier Docker]({filename}../Ateliers/docker_meudon.md), le mardi 8 octobre à Meudon ([informations](https://indico.obspm.fr/event/152/))

* [Atelier Doxygen]({filename}../Ateliers/doxygen_paris.md), le jeudi 21 mars à Paris ([informations](https://indico.obspm.fr/event/69/))

* [Atelier Doxygen]({filename}../Ateliers/doxygen_meudon.md), le jeudi 14 mars à Meudon ([informations](https://indico.obspm.fr/event/68/))

### 2018

* [Atelier HDF5]({filename}../Ateliers/hdf5_paris.md), le lundi 5 novembre à Paris ([informations](https://indico.obspm.fr/event/61/))

* [Atelier HDF5]({filename}../Ateliers/hdf5_meudon.md), le vendredi 23 novembre à Meudon ([informations](https://indico.obspm.fr/event/62/))

* [Atelier Structuration de code et système de compilation GNU Make]({filename}../Ateliers/structuration_makefile_meudon.md), le mardi 29 mars 2018 à Meudon ([informations](https://indico.obspm.fr/event/28/)) 

* [Atelier Structuration de code et système de compilation GNU Make]({filename}../Ateliers/structuration_makefile_paris.md), le mercredi 04 avril 2018 à Paris ([informations](https://indico.obspm.fr/event/29/))

* [Atelier Parallélisme]({filename}../Ateliers/parallelisme_paris.md), le vendredi 16 février 2018 à Paris ([informations](https://indico.obspm.fr/event/26/))

* [Atelier Parallélisme]({filename}../Ateliers/parallelisme_meudon.md), le mardi 30 janvier 2018 à Meudon ([informations](https://indico.obspm.fr/event/25/))

### 2017
* [Atelier Git et gitlab]({filename}../Ateliers/git_gitlab_paris.md), le vendredi 13 octobre 2017 à Paris ([informations](https://indico.obspm.fr/event/23/))

* [Atelier Git et gitlab]({filename}../Ateliers/git_gitlab_meudon.md), le mardi 28 novembre 2017 à Meudon ([informations](https://indico.obspm.fr/event/24/))
