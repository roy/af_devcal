"use strict";

(function(exports){

  function read(data){
    return data;
  }

  // I want to export the json content
  var json_content;
  function get_json_content(){
    return json_content;
  }

  var template = ''+
    '<div>{{&description}}</div>' +
    '<div class="indico_date"> Date :' +
    '  <div class="indico_date_2">  {{startDate.date}} ({{startDate.time}})</div>'+
    '</div>'+
    '<div class="indico_loc"> Lieu :' +
    '  <div class="indico_loc_2">  {{location}}, {{room}}</div>'+
    '</div>'+
    '<div class="indico_chair"> Contributeurs :' +
    '{{#chairs}}'+
    '  <div class="indico_chair_2">{{fullName}}</div>'+
    '{{/chairs}}'+
    '<div class="indico_mat"> Materiel :' +
    '{{#material}}'+
    '  <div class="indico_mat_2">'+
    '  {{#resources}}'+
    '    <a href="{{url}}">{{name}}</a>'+
    '  {{/resources}}'+
    '  </div>'+
    ' {{/material}}'+
    '</div>'+
    '<div class="indico_mat">{{link_name}}'+
    '  <div class="indico_mat_2">'+
    '    <a href="{{url}}" target="_blank" >{{url}}</a>'+
    '  </div>'+
    '</div>'
    ;




  function parse_event(data, nb_event,where){
    let ele = document.getElementById(where),
	element = "indico",
	indico_event = data.results[0];
    json_content = indico_event;

    // manipulate date
    let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' },
	day = new Date(indico_event.startDate.date);
    indico_event.startDate.date = day.toLocaleDateString('fr-FR',options);

    // manipulate link name
    if(new Date() > day){
      // event is past
      indico_event.link_name = ' Lien : ';
    }
    else{
      indico_event.link_name = ' Inscription : ';
    }

    // manipulate time
    let time = indico_event.startDate.time.split(':');   time.pop();
    indico_event.startDate.time = time.join(':');

    // create event
    ele.innerHTML = Mustache.to_html(template,indico_event);
    //console.log(indico_event)

    // remove subtitle in description
    let subtitle = ele.getElementsByTagName('h2');
    if(subtitle.length>0){
      subtitle[0].style.display = "none";
    }

  }


  function read_indico_event(url, nb_event)
  {
    $.ajax( {
      type: "GET",
      url: url + "/export/event/" + nb_event + ".jsonp?callback=read?",
      dataType: "jsonp",
      jsonp: false,
      jsonpCallback: "read",
      success: function(jsonp) {
	$('#indico').show();
      },
      error: function(event) {
	alert("unable to read the event " + nb_event + " from " + url + "/export/event/" + nb_event + ".jsonp?callback=read?");
      },
      complete: function(data){
	parse_event(data.responseJSON, nb_event,'indico');
      }

    });
  }


  let url = $('#indico').html().trim(),
      event = $('#indico_event').html().trim();

  read_indico_event(url,event);

  exports.get_json_content = get_json_content;

})(this.Indico = {});
